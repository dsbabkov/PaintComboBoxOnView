#include "Widget.h"
#include "ui_Widget.h"
#include "Model.h"
#include "Delegate.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    ui->listView->setModel(new Model(this));
    ui->listView->setItemDelegate(new Delegate(this));
}

Widget::~Widget()
{
    delete ui;
}

