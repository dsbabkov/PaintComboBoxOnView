#include "Delegate.h"
#include <QComboBox>
#include <QApplication>
#include <QStyleOptionComboBox>
#include <QPainter>

Delegate::Delegate(QObject *parent)
    : QStyledItemDelegate(parent)
{

}

void Delegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    auto opt = option;
    initStyleOption(&opt, index);

    QStyleOptionComboBox cmbOpt;
    *static_cast<QStyleOption *>(&cmbOpt) = opt;
    cmbOpt.currentText = opt.text;

    QApplication::style()->drawComplexControl(QStyle::CC_ComboBox, &cmbOpt, painter);
    QApplication::style()->drawControl(QStyle::CE_ComboBoxLabel, &cmbOpt, painter);
}

QWidget *Delegate::createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &) const
{
    auto *result = new QComboBox(parent);
    result->addItems({"11111111111111111111",
                      "22222222222222222222"});
    return result;
}

void Delegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    auto *combo = static_cast<QComboBox *>(editor);
    combo->setCurrentText(index.data().toString());
}
