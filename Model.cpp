#include "Model.h"

Model::Model(QObject *parent)
    : QAbstractListModel(parent)
{
}

int Model::rowCount(const QModelIndex &) const
{
    return 2;
}

QVariant Model::data(const QModelIndex &index, int role) const
{
    if (role != Qt::DisplayRole && role != Qt::EditRole) {
        return {};
    }

    return QStringList{
        "11111111111111111111",
        "22222222222222222222"
    }.at(index.row());
}

Qt::ItemFlags Model::flags(const QModelIndex &index) const
{
    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}
